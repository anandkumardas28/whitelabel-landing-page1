<div id="jsSettingsButtonPopout" class="timeline-control-settings__popout">

    <div id="jsQualityMenuItem" class="timeline-control-settings__item" data-setting="quality" tabindex="2">
        <div class="settings-icon settings-quality-icon"></div>
        <div class="timeline-control-settings__text">
            <span class="translate" data-translate="QualityMenuText">Quality</span>
            <span id="jsCurrentQuality" class="setting-light-text js-quality-text"> (Auto)</span>
        </div>
        <div class="settings-arrow-container">
            <div class="setting-arrow right"></div>
        </div>
        <div class="clear-both"></div>
    </div>
    <div id="jsLangMenuItem" class="timeline-control-settings__item" data-setting="language" tabindex="2">
        <div class="settings-icon settings-language-icon"></div>
        <div class="timeline-control-settings__text">
            <span class="translate" data-translate="LanguageMenuText">Language</span>
            <span id="jsCurrentLanguage" class="setting-light-text js-language-text"> (EN)</span>
        </div>
        <div class="settings-arrow-container">
            <div class="setting-arrow right"></div>
        </div>
        <div class="clear-both"></div>
    </div>
    <div id="jsCCMenuItem" class="timeline-control-settings__item" data-setting="closed-captions" tabindex="2">
        <div class="settings-icon settings-cc-icon"></div>
        <div class="timeline-control-settings__text">
            <span class="translate" data-translate="CCMenuText">Closed Captions</span>
            <span id="jsCCValue" class="setting-light-text js-cc-text"> (Off)</span>
        </div>
        <div class="settings-arrow-container">
            <div class="setting-arrow right"></div>
        </div>
        <div class="clear-both"></div>
    </div>

</div>

<?php include_once "partials/language-menu.php" ?>

<?php include_once "partials/cc-menu.php" ?>