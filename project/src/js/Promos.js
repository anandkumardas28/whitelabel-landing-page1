var Promos = {
    initialise: function () {
        Promos.events.initialise();
    },

    promos: {
        promo1: function() {
            VideoPlayerInterface.actions.pause();
            // Log the click event
            VideoPlayerInterface.iframeWindow.rtc.utils.track("promo.click", "promo2");
            window.open("http://www.pitneybowes.com/us/customer-engagement-marketing/synchronized-communications-execution/engageone-video-personalized-video.html", '_blank');
        },

        promo2: function() {
            VideoPlayerInterface.actions.pause();
            // Log the click event
            VideoPlayerInterface.iframeWindow.rtc.utils.track("promo.click", "promo2");
            window.open("http://www.pitneybowes.com/us/customer-engagement-marketing/synchronized-communications-execution/engageone-video-personalized-video.html", '_blank');
        }
    },

    events: {
        initialise: function () {
            $('.promos-container a').click(Promos.events.click);
        },

        click: function(e) {
            Promos.promos[$(this).data('promo')]();
        }
    }
};